`timescale 1ns/1ns

module ascii_art #(

   // Notice provided parameter values are default, in case not provided by instantiating Testbench.
   parameter STR_MAX_LEN = 10,   // Max length of string including termination (can be less, terminated by zero)
   parameter AA_PIX_H    = 10,   // Height of character font.   
   parameter AA_PIX_W    = 25,   // Width of charterer font,
   parameter AA_STRIDE   = 12,   // Staring difference between AA chars (notice chars overlap) -- NOT DRIVEN FROM TB
   parameter AA_MIN_CHAR = " ",  // Min supported ASCII character is space (value 32)
   parameter AA_MAX_CHAR = "_",  // Max supported ASCII character is underscore (value 95)  
   parameter FMEM_ADDR_W = $clog2((AA_MAX_CHAR-AA_MIN_CHAR)*AA_PIX_H*AA_PIX_W) // FMEM Address Width
 )(
    
  // Clock and reset
  input clk ,
  input rst_n,
   
  // String input request   
  input [STR_MAX_LEN-1:0][7:0] str_in, // # Including 0 string termination.
  input str_in_valid, // str_in is valid this cycle
  
  // Fonts pixels memory Interface
  output logic [FMEM_ADDR_W-1:0] fmem_addr, // Address of pixel in font memory
  output logic fmem_rd,                     // Read request
  input  [7:0] fmem_data,                   // Pixel data from menory, available next cycle after rd.
   
  // String output dump (character by charterer, including end of lines.  
  output [7:0] aa_char_out,  // ascii art output character
  output aa_char_out_valid,  // aa_char_out is valid this cycle
  output aa_out_done         // ascii art output done, ready for new string.
);

// ---------------------------------------------------------------------------------------

// CHANGE BELOW YOUR STUDENTS ID
// usedd for random seed by testbench

localparam SUM_STUDENT_ID = (034437962+151934692) ; 

//---------------------------------------------------------------------------------------

// Some helpful calculated parameters

localparam PIX_PER_FONT = AA_PIX_H*AA_PIX_W  ; // Pixels per font
localparam AA_OUT_ARR_MAX_W = ((STR_MAX_LEN-1)*AA_STRIDE)+AA_PIX_W ;

//----------------------------------------------------------------------------------------

// Recommended state-machine states, can be changed if desired
enum logic [2:0] {IDLE, GEN, DUMP, DONE} state , next_state ; 

//---------------------------------------------------------------------------------------

// This 3D packed array holds the ascii-art image to be built and duped
logic [AA_PIX_H-1:0][AA_OUT_ARR_MAX_W-1:0][7:0] aa_image  ; 

// TIP: The way to assign 'space' to all charchters of the array ar reset negedge or re-initilize condition is:
// always @(posedge clk, negedge rst_n) 
// ...
// if (some_condition) aa_image <= {{(AA_PIX_H*AA_OUT_ARR_MAX_W){" "}}} ;  
// ...

//---------------------------------------------------------------------------------------



// ******************** STUDEMTS IMPLMENTATION HERE ********************************



//-----------------------------------------------------------------

// Debug Assistance Task: Print pre-dump output array for debug assistance

task dump_img_to_file ; 
  int ri,ci,dbgf ;
  logic [7:0] pix ;
begin
  $display("For debug assistance see intermediate image array content (before dump) in generated file: dbg_aa_display.txt");
  dbgf = $fopen("dbg_aa_display.txt","w");
  $fdisplay(dbgf,"\nPrinting for debug assistance only the pre-dumped output array");
  $fdisplay(dbgf,"This is NOT the actual dumped ascii art to be printed by the Testbench\n");  
   for (ri=0;ri<AA_PIX_H;ri++) begin  
     $fwrite(dbgf,"%1d:",ri); 
     for (ci=0;ci<AA_OUT_ARR_MAX_W;ci++) begin       
       pix = aa_image[ri][ci] ;
       $fwrite(dbgf,"%c", pix);
     end       
     $fwrite(dbgf,"\n");
   end
   $fwrite(dbgf,"  ");
   for (ci=0;ci<100;ci++) $fwrite(dbgf,"%1d",ci/10); 
   $fwrite(dbgf,"\n  ");   
   for (ci=0;ci<100;ci++) $fwrite(dbgf,"%1d",ci%10);
   $fwrite(dbgf,"\n");
   $fclose(dbgf);
end
endtask

//------------------------------------------------------------------

endmodule 






