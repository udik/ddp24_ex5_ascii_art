`timescale 1ns/1ns

module ascii_art_tb ();
    
   parameter STR_MAX_LEN = 16;   // Max length of string including termination (can be less, terminated by zero)
   parameter AA_PIX_H    = 10;   // Height of character font.   
   parameter AA_PIX_W    = 25;   // Width of charterer font
 //parameter AA_STRIDE   = 10;   // Staring difference between AA chars (notice chars overlap) [Can be defined by DUT]
   parameter AA_MIN_CHAR = " ";  // Min supported ASCII character is space (value 32)
   parameter AA_MAX_CHAR = "_";  // Max supported ASCII character is underscore (value 95) 
   parameter FMEM_SIZE   =  ((AA_MAX_CHAR-AA_MIN_CHAR)*AA_PIX_H*AA_PIX_W) ; // font memory size
   parameter FMEM_ADDR_W = ($clog2(FMEM_SIZE)); // font memory Address Width

//----------------------------------------------------------------------------------

  localparam HALF_CLK_PERIOD_NS = 5 ;
  localparam CLK_PERIOD_NS = (2*HALF_CLK_PERIOD_NS) ;
  
//---------------------------------------------------------------------------------
  logic clk;
  logic rst_n;
  
  // String input request  
  //string  aa_str ; 
  logic [STR_MAX_LEN-1:0][7:0] str_in ; // # Including 0 string termination.
  logic str_in_valid; // str_in is valid this cycle
  
  // Fonts pixels memory Interface
  logic [FMEM_ADDR_W-1:0] fmem_addr; // Address of pixel in font memory
  logic fmem_rd; // Read request
  logic [7:0] fmem_data; // Pixel data from menory, available next cycle after rd.
   
  // String output dump (character by charterer, including end of lines.  
  logic [7:0] aa_char_out;  // ascii art output character
  logic aa_char_out_valid;  // aa_char_out is valid this cycle
  logic aa_out_done;    // ascii art output done, ready for new string.
  
  // font memory
  reg [7:0] fmem [0:FMEM_SIZE-1];
  
  int aa_dumpF ;
  
//----------------------------------------------------------------------------------

 ascii_art #(
   .STR_MAX_LEN (STR_MAX_LEN), // Max length of string including termination (can be less, terminated by zero)
   .AA_PIX_H    (AA_PIX_H   ), // Height of character font.   
   .AA_PIX_W    (AA_PIX_W   ), // Width of charterer font
  //.AA_STRIDE   (AA_STRIDE  ), // Staring difference between AA chars (notice chars overlap) [Can be defined by DUT]
   .AA_MIN_CHAR (AA_MIN_CHAR), // Min supported ASCII character is space (value 32)
   .AA_MAX_CHAR (AA_MAX_CHAR), // Max supported ASCII character is underscore (value     
   .FMEM_ADDR_W (FMEM_ADDR_W)  // FMEM Address Width
  ) ascii_art (
    
  // Clock and reset
  .clk   (clk) ,
  .rst_n (rst_n),
   
  // String input request   
  .str_in      (str_in),      // # Including 0 string termination.
  .str_in_valid (str_in_valid), // str_in is valid this cycle
  
  // Fonts pixels memory Interface
  .fmem_addr   (fmem_addr), // Address of pixel in font memory
  .fmem_rd     (fmem_rd),   // Read request
  .fmem_data   (fmem_data), // Pixel data from menory, available next cycle after rd.
   
  // String output dump (character by charterer, including end of lines.  
  .aa_char_out         (aa_char_out),       // ascii art output character
  .aa_char_out_valid   (aa_char_out_valid), // aa_char_out is valid this cycle
  .aa_out_done         (aa_out_done)        // ascii art output done, ready for new string.
);

//----------------------------------------------------------------------------------

 // Font Memory access
 always @(posedge clk, negedge rst_n)
 if (!rst_n) fmem_data <= 0 ;
 else begin 
   if (fmem_rd) fmem_data <= fmem[fmem_addr] ;
 end
 
 // Display
 always @(posedge clk) if (aa_char_out_valid) begin
  $write("%1c",aa_char_out) ;
  $fwrite(aa_dumpF,"%1c",aa_char_out) ;
 end

//-------------------------------------------------------------------------------

// testbench ascii_art request task

task req_aa(string  aa_str) ; 
begin
    foreach (aa_str[i]) str_in[i] = aa_str[i] ;
    str_in_valid = 1 ;
    #10 ; 
    str_in = 0 ;
    str_in_valid = 0 ; 
    wait(aa_out_done==1) ;
    $fflush();
    #25 ;   
end
endtask

//-------------------------------------------------------------------------------

// Pool of testbench strings

localparam NUM_STRINGS = 19 ;
static string tb_strings_pool[NUM_STRINGS] = 
{"COOL","CASUAL","GREAT","SUPERB","PERFECT","AWESOME","TREMENDOUS",
 "MARVELOUS","EXCELLENT","WOW","LOVELY","WONDERFUL","FABULOUS",
 "GORGEOUS","PRETTY","ELEGANT","CUTE","WELL DONE","PLEASANT"};


//-------------------------------------------------------------------------------

// Execution
int si;
int rnd_str_idx;
initial clk = 1 ;
always #(CLK_PERIOD_NS/2) clk = !clk ;
   
initial begin
    $display("Notice: output dumped ascii_art alo captured to file: aa_tb_dump.txt");
    aa_dumpF = $fopen("aa_tb_dump.txt","w");
    $readmemh("../fonts_lib/aa_fmem_readmemh.txt", fmem); // Load fonts memory
    str_in = 0 ;
    str_in_valid = 0 ;        
    rst_n = 1'b1;   
	#(5*CLK_PERIOD_NS) ;     
    rst_n = 1'b0;
	#((4*CLK_PERIOD_NS)+HALF_CLK_PERIOD_NS) ;           
    rst_n = 1'b1;  
    #20 ;
    rnd_str_idx = (ascii_art.SUM_STUDENT_ID+99991)%NUM_STRINGS ; // for unique pseudo random seed per students team.  
    for (si=0;si<3;si++) begin
      rnd_str_idx = (rnd_str_idx+$urandom_range(23399,0))%NUM_STRINGS ;
      req_aa(tb_strings_pool[rnd_str_idx]) ;
    end            
    $fclose(aa_dumpF);
    $finish;
 end
 
 
//---------------------------------------------------------------------------------- 

endmodule 






